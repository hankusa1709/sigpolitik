import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Ionicons, AntDesign } from '@expo/vector-icons';
import { Provider as PaperProvider } from 'react-native-paper';

// Auth Screen
import LoginScreen from './screens/auth/LoginScreen';
import SignUpScreen from './screens/auth/SignUpScreen';
import Dashboard from './screens/auth/Dashboard'

// Question Screen
import AddQuestionScreen from './screens/module/Questions/AddQuestionScreen';
import QuestionScreen from './screens/module/Questions/QuestionScreen';
import QuestionDetailScreen from './screens/module/Questions/QuestionDetailScreen';

// Survey Screen
import SurveyScreen from './screens/module/Survey/SurveyScreen';
import SurveyedScreen from './screens/module/Survey/SurveyedScreen'

// Component
import CameraScreen from './screens/components/Camera'
import Statistic from './screens/components/Statistic'

import {decode, encode} from 'base-64'

if (!global.btoa) {  global.btoa = encode }

if (!global.atob) { global.atob = decode }

const Stack = createStackNavigator();

function HomeScreen() {
  return (
    <Stack.Navigator>
      <Stack.Screen 
      name="SurveyedScreen" 
      component={SurveyedScreen} 
      options={{ title: 'Lembar Kontrol', headerLeft: null }}
      />
      <Stack.Screen 
      name="SurveyScreen"
      component={SurveyScreen} 
      options={{ title: 'Lembar Survei' }}
      />
    </Stack.Navigator>
  );
}

function SettingsScreen() {
  return (
    <Stack.Navigator>
      <Stack.Screen 
        name="AddQuestionScreen" 
        component={AddQuestionScreen} 
        options={{ title: 'Tambah Pertanyaan' }}
      />
      <Stack.Screen 
        name="QuestionScreen"
        component={QuestionScreen} 
        options={{ title: 'Pertanyaan' }}
      />
      <Stack.Screen 
        name="QuestionDetailScreen" 
        component={QuestionDetailScreen} 
        options={{ title: 'Edit Pertanyaan' }}
      />
    </Stack.Navigator>
  );
}

function MyStack() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
      />
      <Stack.Screen
        name="SignUpScreen"
        component={SignUpScreen}
      />
      <Stack.Screen 
       name="Dashboard" 
       component={Dashboard} 
      />
    </Stack.Navigator>
  );
}

const Drawer = createDrawerNavigator();

function DrawerNav() {
  return (
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen name="Home" component={HomeTabs} />
      <Drawer.Screen name="Profile" component={Dashboard} />
      <Drawer.Screen name="Camera" component={CameraScreen} />
    </Drawer.Navigator>
  );
}

function HomeTabs() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          let iconType;
          if (route.name === 'Beranda') {
            iconType = 'Ionicons'
            iconName = 'ios-home';
          } else if (route.name === 'Pertanyaan') {
            iconType = 'Ionicons'
            iconName = 'ios-list';
          } else if (route.name === 'Statistic') {
            iconType = 'AntDesign'
            iconName = 'linechart';
          }
          // You can return any component that you like here!
          if (iconType === 'Ionicons') {
            return <Ionicons name={iconName} size={size} color={color} />;
          } else if (iconType === 'AntDesign') {
            return <AntDesign name={iconName} size={size} color={color} />;
          }
        },
      })}
      tabBarOptions={{
        activeTintColor: '#187bcd',
        inactiveTintColor: 'gray',
      }}
    >
      <Tab.Screen name="Beranda" component={HomeScreen} />
      <Tab.Screen name="Pertanyaan" component={SettingsScreen} />
      <Tab.Screen name="Statistic" component={Statistic} />
    </Tab.Navigator>
  );
}

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <PaperProvider>
      <NavigationContainer>
        <Stack.Navigator headerMode="none">
          <Stack.Screen name="Akun" component={MyStack} />
          <Stack.Screen name="Draw" component={DrawerNav} />
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
}