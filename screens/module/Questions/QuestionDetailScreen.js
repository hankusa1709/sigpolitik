import React, { Component } from 'react';
import { Alert, Text, Button, StyleSheet, TextInput, ScrollView, ActivityIndicator, View } from 'react-native';
import firebase from '../../../database/firebaseDb';

class QuestionDetailScreen extends Component {

  constructor() {
    super();
    this.state = {
      question: '',
      a: '',
      b: '',
      c: '',
      d: '',
      isLoading: true
    };
  }
 
  componentDidMount() {
    const dbRef = firebase.firestore().collection('questions').doc(this.props.route.params.userkey)
    dbRef.get().then((res) => {
      if (res.exists) {
        const data = res.data();
        this.setState({
          key: res.id,
          question: data.question,
          a: data.a,
          b: data.b,
          c: data.c,
          d: data.d,
          isLoading: false
        });
      } else {
        console.log("Document does not exist!");
      }
    });
  }

  inputValueUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  updateUser() {
    this.setState({
      isLoading: true,
    });
    const updateDBRef = firebase.firestore().collection('questions').doc(this.state.key);
    updateDBRef.set({
      question: this.state.question,
      a: this.state.a,
      b: this.state.b,
      c: this.state.c,
      d: this.state.d,
    }).then((docRef) => {
      this.setState({
        key: '',
        question: '',
        a: '',
        b: '',
        c: '',
        d: '',
        isLoading: false,
      });
      this.props.navigation.navigate('QuestionScreen');
    })
    .catch((error) => {
      console.error("Error: ", error);
      this.setState({
        isLoading: false,
      });
    });
  }

  deleteUser() {
    const dbRef = firebase.firestore().collection('questions').doc(this.props.route.params.userkey)
      dbRef.delete().then((res) => {
          console.log('Item removed from database')
          this.props.navigation.navigate('QuestionScreen');
      })
  }

  openTwoButtonAlert=()=>{
    Alert.alert(
      'Delete User',
      'Are you sure?',
      [
        {text: 'Yes', onPress: () => this.deleteUser()},
        {text: 'No', onPress: () => console.log('No item was removed'), style: 'cancel'},
      ],
      { 
        cancelable: true 
      }
    );
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }
    return (
      <ScrollView style={styles.container}>
        <View style={styles.inputGroup}>
          <Text>
            Pertanyaan :
          </Text>
          <TextInput
              placeholder={'Pertanyaan'}
              value={this.state.question}
              onChangeText={(val) => this.inputValueUpdate(val, 'question')}
              />
        </View>
        <View style={styles.inputGroup}>
              <Text>
                A :
              </Text>
          <TextInput
              placeholder={'A'}
              value={this.state.a}
              onChangeText={(val) => this.inputValueUpdate(val, 'a')}
              />
        </View>
        <View style={styles.inputGroup}>
              <Text>
                B :
              </Text>
          <TextInput
              placeholder={'B'}
              value={this.state.b}
              onChangeText={(val) => this.inputValueUpdate(val, 'b')}
              />
        </View>
        <View style={styles.inputGroup}>
              <Text>
                C :
              </Text>
          <TextInput
              placeholder={'C'}
              value={this.state.c}
              onChangeText={(val) => this.inputValueUpdate(val, 'c')}
              />
        </View>
        <View style={styles.inputGroup}>
              <Text>
                D :
              </Text>
          <TextInput
              placeholder={'D'}
              value={this.state.d}
              onChangeText={(val) => this.inputValueUpdate(val, 'd')}
          />
        </View>
        <View style={styles.button}>
          <Button
            title='Update'
            onPress={() => this.updateUser()} 
            color="#4ab19d"
          />
          </View>
         <View>
          <Button
            title='Delete'
            onPress={() => this.deleteUser()}
            color="#ef3d59"
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35
  },
  inputGroup: {
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    marginBottom: 7,
  },
  text: {
    marginRight: 20
  }
})

export default QuestionDetailScreen;