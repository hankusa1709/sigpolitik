// screens/UserScreen.js

import React, { Component } from 'react'
import { StyleSheet, ScrollView, ActivityIndicator, View, Text } from 'react-native'
import { Provider as PaperProvider } from 'react-native-paper'
import { Card, Title, Paragraph, RadioButton, Divider, Button, Dialog, Portal } from 'react-native-paper'
import firebase from '../../../database/firebaseDb'

class SurveyScreen extends Component {
  constructor() {
    super();
    this.userRef = firebase.firestore().collection('users');
    this.firestoreRef = firebase.firestore().collection('questions');
    this.state = {
      check: false,
      isLoading: true,
      userArr: [],
      nik: '',
      visible: false,
      selesai: false
    }
  }

  _hideDialog = () => this.setState({ visible: false, selesai: false })

  cancel() {
    this.setState({
      visible: true
    })
  }

  loadParams() {
    const dbRef = firebase.firestore().collection('users').doc(this.props.route.params.userkey)
    dbRef.get().then((res) => {
      if (res.exists) {
        const data = res.data()
        this.setState({
          key: res.id,
          nik: data.nik,
          isLoading: false
        })
      } else {
        console.log("Document does not exist!")
      }
    })
  }

  back() {
    this.props.navigation.navigate('SurveyedScreen')
  }

  complete() {
    this.setState({
      selesai: true
    })
  }

  selesai() {
    this.setState({
      isLoading: true,
    })
    const updateDBRef = firebase.firestore().collection('users').doc(this.state.key)
    updateDBRef.set({
      nik: this.state.nik,
      status: true,
    }).then((docRef) => {
      this.setState({
        selesai: false,
        isLoading: false,
      })
      this.props.navigation.navigate('SurveyedScreen')
    })
    .catch((error) => {
      console.error("Error: ", error);
      this.setState({
        isLoading: false,
      })
    })
  }

  componentDidMount() {
    this.loadParams()
    this.unsubscribe = this.firestoreRef.onSnapshot(this.getCollection)
  }

  componentWillUnmount(){
    this.loadParams()
    this.unsubscribe()
  }

  getCollection = (querySnapshot) => {
    const userArr = []
    querySnapshot.forEach((res) => {
      const { question, a, b, c, d } = res.data()
      userArr.push({
        key: res.id,
        res,
        question,
        a,
        b,
        c,
        d,
      })
    })
    this.setState({
      userArr,
      isLoading: false,
   })
  }

  check() {
    this.setState({
      check: true
    })
  }
  
  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large"/>
        </View>
      )
    }    
    return (
      <ScrollView style={styles.container}>
      <Portal>
        <Dialog
          visible={this.state.visible}
          onDismiss={this._hideDialog}>
          <Dialog.Title>Apakah Anda Yakin?</Dialog.Title>
          <Dialog.Content>
            <Paragraph>Seluruh Data Tidak akan tersimpan</Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={() => this._hideDialog()}>Tidak</Button>
            <Button onPress={() => this.back()}>Iya</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      <Portal>
        <Dialog
          visible={this.state.selesai}
          onDismiss={this._hideDialog}>
          <Dialog.Title>Apakah Anda Yakin?</Dialog.Title>
          <Dialog.Content>
            <Paragraph>Seluruh Data Akan Tersimpan dan Tak Dapat Diubah</Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={() => this._hideDialog()}>Tidak</Button>
            <Button onPress={() => this.selesai()}>Iya</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
        <Card>
          <Card.Content>
            <Paragraph>NIK : { this.state.nik }</Paragraph>
          </Card.Content>
        </Card>
        <Divider />
        <Card>
          {
            this.state.userArr.map((item, i) => {
              return (
                  <Card.Content>
                   <Title>{ i + 1}.{ item.question }</Title>
                      <RadioButton.Group
                        onValueChange={value => this.setState({ value })}
                        value={this.state.value}
                      >
                        <View>
                          <Text>{ item.a }</Text>
                          <RadioButton value={ item.a } />
                        </View>
                        <View>
                          <Text>{ item.b }</Text>
                          <RadioButton value={ item.b } />
                        </View>
                        <View>
                          <Text>{ item.c }</Text>
                          <RadioButton value={ item.c } />
                        </View>
                        <View>
                          <Text>{ item.d }</Text>
                          <RadioButton value={ item.d } />
                        </View>
                      </RadioButton.Group>
                  </Card.Content>
              )
            })
          }
          <Card.Actions style={{ justifyContent:"center" }}>
          <Button style={styles.button} mode="contained" onPress={() => this.cancel()}>
            Kembali
          </Button>
          <Button style={styles.button} mode="contained" onPress={() => this.complete()}>
            Selesai
          </Button>
          </Card.Actions>
        </Card>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingBottom: 22
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    marginHorizontal: 20,
  }
})

export default SurveyScreen