// screens/UserScreen.js

import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, View, Alert, PixelRatio } from 'react-native';
import { Card, Title, Paragraph, Divider, TextInput, Button, DataTable, Surface, Dialog, Portal, ActivityIndicator, Colors } from 'react-native-paper';
import firebase from '../../../database/firebaseDb';
import { Ionicons } from '@expo/vector-icons';
import { firestore } from 'firebase';
import { Provider as PaperProvider } from 'react-native-paper';
import moment from 'moment'

var idLocale = require('moment/locale/id'); 
moment.locale('id', idLocale);

var FONT_BACK_LABEL = 60;

if (PixelRatio.get() <= 2) {
  FONT_BACK_LABEL = 200;
}

class SurveyedScreen extends Component {

  constructor() {
    super();
    this.firestoreRef = firebase.firestore().collection('users');
    this.state = {
      check: false,
      isLoading: true,
      userArr: [],
      res: '',
      total: '',
      size: '',
      visible: false,
      loadSize: false
    };
  }

  _hideDialog = () => this.setState({ visible: false });
  
  _showDialog = () => this.setState({ visible: true });

  componentWillMount() {
    this.setState({ nik: '' })
    this.countNik();
    this.size();
  }

  storeNik() {
    if(this.state.nik === ''){
     this._showDialog()
    } else {
      this.setState({
        isLoading: true,
      });      
      this.firestoreRef.add({
        status: false,
        date_add: firebase.firestore.FieldValue.serverTimestamp(),
        nik: this.state.nik,
      }).then((res) => {
        this.setState({
          nik: '',
          status: false,
        });
        this.countNik();
        this.size();
      })
      .catch((err) => {
        console.error("Error found: ", err);
        this.setState({
          isLoading: false,
        });
      });
    }
  }

  countNik() {
    this.setState({
      isLoading: true,
    });
    firestore()
    .collection('users')
    // Filter
    .where('status', '==', true)
    .get()
    .then(querySnapshot => {
      console.log('data:', querySnapshot.size)
      this.setState({
        total: querySnapshot.size,
        isLoading: false,
      })
    })
    }

    isiSurvey() {
      this.props.navigation.navigate('SurveyScreen', {
        userkey: item.key
      });
    }

  updateInputVal = (nik) => {
    let newText = '';
    let numbers = '0123456789';

    for (var i=0; i < nik.length; i++) {
        if(numbers.indexOf(nik[i]) > -1 ) {
            newText = newText + nik[i];
        }
        else {
            // your call back function
        this._showDialog()
        }
    }
    this.setState({ nik: newText });
  }

  size() {
    this.setState({
      loadSize: true,
    });
    firestore()
    .collection('users')
    // Filter
    .get()
    .then(querySnapshot => {
      console.log('data:', querySnapshot.size)
      this.setState({
        size: querySnapshot.size,
        loadSize: false,
      })
    })
  }

  componentDidMount() {
      // this.countNik();
      this.size();
      this.unsubscribe = this.firestoreRef.onSnapshot(this.getCollection);
    }
    
    componentWillUnmount(){
    this.size();
    this.countNik();
    this.unsubscribe();
  }

  getCollection = (querySnapshot) => {
    const userArr = [];
    querySnapshot.forEach((res) => {
      const { nik, status, date_add } = res.data();
      userArr.push({
        key: res.id,
        res,
        nik,
        status,
        date_add
      });
    });
    this.setState({
      userArr,
      isLoading: false,
   });
  }

  check() {
    this.setState({
      check: true
    })
  }

  render() {
    if(this.state.isLoading){
      return(
        <ActivityIndicator size="large" style={styles.preloader} animating={true} color={Colors.blue400} />
      )
    }    
    return (
      <PaperProvider>
      <Portal>
        <Dialog
          visible={this.state.visible}
          onDismiss={this._hideDialog}>
          <Dialog.Title>Error!</Dialog.Title>
          <Dialog.Content>
            { this.state.nik === '' ? 
              <Paragraph>NIK Tidak Boleh Kosong!</Paragraph> :
              <Paragraph>Masukkan Hanya Angka Saja!</Paragraph>
            }
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={() => this._hideDialog()}>Ok</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      <ScrollView style={styles.container}>
        <Title style={styles.title}>Update Terkini Survei Politik</Title>
        <Surface style={styles.surface}>
          { this.state.loadSize === false ?
            <Text style={styles.text}>{ this.state.size }</Text> :
            <ActivityIndicator size="large" style={styles.preloader} animating={true} color={Colors.blue400} />
          }
          <Text style={styles.survei}>Tersurvei</Text>
        </Surface>
        <Surface style={styles.camera}>
          <Button
            style={styles.cameraButton}
            onPress={() => this.props.navigation.navigate('Camera')}
            mode="contained"
            >
            Scan E-KTP
          </Button>
        </Surface>
        <Surface style={styles.card}>
          <TextInput
            style={styles.inputStyle}
            keyboardType = 'numeric'
            label="NIK"
            value={this.state.nik}
            onChangeText={(nik) => this.updateInputVal(nik)}
            maxLength={16}
          />
        <Button
          style={styles.button}
          onPress={() => this.storeNik()}
          mode="contained"
          >
          Input
        </Button>
        </Surface>
        <Divider />
        <DataTable>
          <DataTable.Header>
            <DataTable.Title style={styles.no}>No</DataTable.Title>
            <DataTable.Title>NIK</DataTable.Title>
            <DataTable.Title>Status</DataTable.Title>
            <DataTable.Title>Tgl. Ditambahkan</DataTable.Title>
          </DataTable.Header>
          {
            this.state.userArr.map((item, i) => {
              return (
                <DataTable.Row key={i}>
                  <DataTable.Cell style={styles.no}>{ i + 1 }</DataTable.Cell>
                  <DataTable.Cell>{ item.nik }</DataTable.Cell>
              { item.status === false ? 
                  <DataTable.Cell onPress={() => {this.props.navigation.navigate('SurveyScreen', { userkey: item.key });}}>Survei</DataTable.Cell> : 
                  <DataTable.Cell style={styles.done}>SELESAI</DataTable.Cell>
              }
                  <DataTable.Cell>{ item.date_add && moment(item.date_add.toDate()).calendar() }</DataTable.Cell>
                </DataTable.Row>
              );
            })
          }
          <DataTable.Pagination
          page={1}
          numberOfPages={3}
          onPageChange={(page) => { console.log(page); }}
          label="1 of 6"
        />
          </DataTable>
          {/* <CameraRN /> */}
      </ScrollView>
      </PaperProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   paddingBottom: 22
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  },
  done : {
    color: '#E33A3A',
    justifyContent: "center"
  },
  unDone: {
    color: '#E33A3A',
    justifyContent: "center"
  },
  card: {
    marginBottom: 20,
    padding: 3,
    height: 120,
    width: 320,
    alignSelf: 'center',
    justifyContent: 'center',
    elevation: 4,
    borderRadius: 5,
    flexDirection: "row"
  },
  camera: {
    marginBottom: 20,
    padding: 3,
    height: 120,
    width: 320,
    alignSelf: 'center',
    justifyContent: 'center',
    elevation: 4,
    borderRadius: 5,
    flexDirection: "row"
  },
  surface: {
    marginTop: 5,
    marginBottom: 20,
    padding: 8,
    height: 180,
    width: 320,
    alignSelf: 'center',
    justifyContent: 'center',
    elevation: 4,
    borderRadius: 5
  },
  inputStyle: {
    backgroundColor: 'white',
    width: 180,
    height: 100,
    marginBottom: 30,
    marginRight: 25,
  },
  text: {
    fontSize: 55,
    fontWeight: 'bold',
    marginLeft: 40
  },
  button: {
    height: 50,
    width: 80,
    alignSelf:"center",
    borderRadius: 5,
    justifyContent:'center',
    marginTop: 15
  },
  cameraButton: {
    height: 50,
    width: 200,
    alignSelf:"center",
    borderRadius: 5,
    justifyContent:'center'
  },
  title: {
    marginTop: 5,
    marginLeft: 23,
    fontSize: 20
  },
  actions: {
    alignSelf:"center",
  },
  no: {
    width: 5
  },
  survei: {
    textAlign: "right",
    marginRight: 20,
    marginTop: 20
  }
})

export default SurveyedScreen;