import React, { Component } from 'react';
import { StyleSheet, View, Alert, ActivityIndicator } from 'react-native';
import { Button, Snackbar, RadioButton, Text, TextInput, Title } from 'react-native-paper';
import firebase from '../../database/firebaseDb';


class SignUpScreen extends Component {
  
  constructor() {
    super();
    this.dbRef = firebase.firestore().collection('users');
    this.state = {
      displayName: '',
      email: '', 
      password: '',
      isLoading: false
    }
  }

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  registerUser = () => {
    if(this.state.email === '' && this.state.password === '') {
      Alert.alert('Enter details to signup!')
    } else {
      this.setState({
        isLoading: true,
      })
      firebase
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then((res) => {
        res.user.updateProfile({
          displayName: this.state.displayName
        })
        console.log('Data', res)
        this.setState({
          isLoading: false,
          displayName: '',
          email: '', 
          password: '',
        })
        this.props.navigation.navigate('LoginScreen')
      })
      .catch(error => this.setState({ errorMessage: error.message }))
    }
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }    
    return (
      <View style={styles.container}>
        <Title style={styles.title}>Sign Up</Title>
        <TextInput
          style={styles.inputStyle}
          placeholder="Name"
          value={this.state.displayName}
          onChangeText={(val) => this.updateInputVal(val, 'displayName')}
        />
        <TextInput
          style={styles.inputStyle}
          placeholder="Email"
          value={this.state.email}
          onChangeText={(val) => this.updateInputVal(val, 'email')}
        />
        <TextInput
          style={styles.inputStyle}
          placeholder="Password"
          value={this.state.password}
          onChangeText={(val) => this.updateInputVal(val, 'password')}
          maxLength={15}
          secureTextEntry={true}
        />   
        <Button
        style={styles.button}
        onPress={() => this.registerUser()}
        mode="contained"
        >
         Sign Up
        </Button>

        <Text 
          style={styles.loginText}
          onPress={() => this.props.navigation.navigate('LoginScreen')}>
          Already Registered? Click here to Login
        </Text>                          
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: 35,
    backgroundColor: '#fff'
  },
  inputStyle: {
    backgroundColor: 'white',
    marginBottom: 15,
  },
  loginText: {
    color: '#4AB19D',
    marginTop: 25,
    textAlign: 'center'
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  title: {
    textAlign: "center",
    marginBottom: 50,
    fontSize: 24
  },
  button: {
    height: 50,
    marginTop: 20,
    borderRadius: 5,
    justifyContent:'center'
  }
});
export default SignUpScreen;