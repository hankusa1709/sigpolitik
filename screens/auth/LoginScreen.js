// components/login.js

import React, { Component } from 'react';
import { StyleSheet, View, Alert } from 'react-native';
import { Button, Snackbar, RadioButton, Text, TextInput, Title, ActivityIndicator, Colors, Paragraph, Dialog, Portal } from 'react-native-paper';
import firebase from '../../database/firebaseDb';


class LoginScreen extends Component {
  
  constructor() {
    super();
    this.state = { 
      email: '', 
      password: '',
      isLoading: false,
      showToast: false,
      message: ''
    }
  }

  _showDialog = () => this.setState({ visible: true });

  _hideDialog = () => this.setState({ visible: false });

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  userLogin = () => {
    if(this.state.email === '' || this.state.password === '') {
      this.setState({
        message: 'Dimohon Untuk Melengkapi Form'
      })
      this._showDialog()
    } else {
      this.setState({
        isLoading: true,
      })
      firebase
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then((res) => {
        console.log(res)
        console.log('User logged-in successfully!', res)
        this.setState({
          isLoading: false,
          email: '', 
          password: ''
        })
        this.props.navigation.navigate('Draw')
      })
      .catch((error) => {
        this.setState({
          message: 'Email atau Password Salah!',
          isLoading: false
        })
        this._showDialog()
        this.props.navigation.navigate('LoginScreen')
      })
    }
  }

  render() {
    if(this.state.isLoading){
      return(
        <ActivityIndicator size="large" style={styles.preloader} animating={true} color={Colors.blue400} />
      )
    }    
    return (
      <View style={styles.container}>
        <Portal>
          <Dialog
             visible={this.state.visible}
             onDismiss={this._hideDialog}>
            <Dialog.Title>Error</Dialog.Title>
            <Dialog.Content>
              <Paragraph>{ this.state.message }</Paragraph>
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={this._hideDialog}>Ok</Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
        <Title style={styles.title}>Login</Title>
        <TextInput
          style={styles.inputStyle}
          label="Email"
          value={this.state.email}
          onChangeText={(val) => this.updateInputVal(val, 'email')}
        />
        <TextInput
          style={styles.inputStyle}
          label="Password"
          value={this.state.password}
          onChangeText={(val) => this.updateInputVal(val, 'password')}
          secureTextEntry={true}
        />
        <Button
        style={styles.button}
        onPress={() => this.userLogin()}
        mode="contained"
        >
         Login
        </Button>
        <Text 
          style={styles.loginText}
          onPress={() => this.props.navigation.navigate('SignUpScreen')}>
          Don't have account? Click here to Sign Up
        </Text>              
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: 35,
    backgroundColor: '#fff'
  },
  inputStyle: {
    backgroundColor: 'white',
    marginBottom: 15,
  },
  loginText: {
    color: '#4AB19D',
    marginTop: 25,
    textAlign: 'center'
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  title: {
    textAlign: "center",
    marginBottom: 50,
    fontSize: 24
  },
  button: {
    height: 50,
    marginTop: 20,
    borderRadius: 5,
    justifyContent:'center'
  }
});
export default LoginScreen;