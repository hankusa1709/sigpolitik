import React, { Component } from 'react';
import { PieChart } from "react-native-chart-kit";
import { Surface, Text, Title } from 'react-native-paper';
import { StyleSheet, View, Dimensions } from 'react-native'

const screenWidth = Dimensions.get("window").width;

const chartConfig = {
    backgroundGradientFrom: "#1E2923",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#08130D",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false // optional
  };

const kabupaten = [
    {
      name: "Kab. Bandung",
      population: 30,
      color: "#03254c",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    },
    {
      name: "Kab. Bekasi",
      population: 20,
      color: "#1167b1",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    },
    {
      name: "Kab. Bogor",
      population: 50,
      color: "#187bcd",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    },
    {
      name: "Kab. Cimahi",
      population: 30,
      color: "#2a9df4",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    },
    {
      name: "Kab. Depok",
      population: 60,
      color: "#d0efff",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    }
  ]
 
  const kota = [
    {
      name: "Bandung",
      population: 56,
      color: "#ffbaba",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    },
    {
      name: "Bekasi",
      population: 50,
      color: "#ff7b7b",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    },
    {
      name: "Bogor",
      population: 93,
      color: "#ff5252",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    },
    {
      name: "Cimahi",
      population: 63,
      color: "#ff0000",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    },
    {
      name: "Depok",
      population: 78,
      color: "#a70000",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    }
  ]

class Statistic extends Component {
    
      render() {
          return (
              <View>
                  <Surface style={styles.surface}>
                    <Text style={styles.title}>Statistik Kota</Text>
                  <PieChart
                  style={styles.chart}
                  data={kota}
                  width={screenWidth}
                  height={150}
                  chartConfig={chartConfig}
                  accessor="population"
                  backgroundColor="transparent"
                  paddingLeft="15"
                  absolute
                />
                  </Surface>
                  <Surface style={styles.surface}>
                    <Text style={styles.title}>Statistik Kabupaten</Text>
                  <PieChart
                  style={styles.chart}
                  data={kabupaten}
                  width={screenWidth}
                  height={150}
                  chartConfig={chartConfig}
                  accessor="population"
                  backgroundColor="transparent"
                  paddingLeft="-7"
                  absolute
                />
                  </Surface>
              </View>
          );
      }
    }
 
const styles = StyleSheet.create({
  container: { alignItems: 'center', justifyContent: 'center', height: 1050 },
  gauge: {
    position: 'absolute',
    width: 100,
    height: 160,
    alignItems: 'center',
    justifyContent: 'center',
  },
  gaugeText: {
    backgroundColor: 'transparent',
    color: '#000',
    fontSize: 24,
  },
  surface: {
    marginTop: 30,
    padding: 8,
    height: 260,
    width: 335,
    alignSelf: 'center',
    elevation: 4,
    borderRadius: 5
  },
  chart: {
      alignSelf: "center",
      marginTop: 25
  },
  title: {
      textAlign: "center",
      fontSize: 25,
      marginTop: 10
  }
})

export default Statistic;